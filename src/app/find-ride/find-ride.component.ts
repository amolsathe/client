import { Component, OnInit, NgZone, ViewChild, ElementRef } from '@angular/core';
import { MouseEvent, MapsAPILoader, MarkerManager } from '@agm/core';
//import {  } from 'googlemaps';
import { FormControl } from '@angular/forms';
//import { google } from '@agm/core/services/google-maps-types';

@Component({
  selector: 'app-find-ride',
  templateUrl: './find-ride.component.html',
  styleUrls: ['./find-ride.component.css']
})
export class FindRideComponent implements OnInit {

  @ViewChild('searchSrc')
  public searchElementRefSrc:ElementRef;
  @ViewChild('searchDest')
  public searchElementRefDest:ElementRef;

  source:string="";
  destination:string="";
  latlngs:any = [];
  origin:any=[];
  dest:any;
  latlng:any = {};
  public searchControl:FormControl;
  private geoCoder;
  address: any;
  constructor(private mapAPILoader:MapsAPILoader, private ngZone:NgZone) {
  }

  ngOnInit() {
    this.searchControl = new FormControl();
    this.mapAPILoader.load().then(()=>{
      this.setCurrentPosition();
      this.geoCoder = new google.maps.Geocoder;

      let autocompleteSrc = new google.maps.places.Autocomplete(this.searchElementRefSrc.nativeElement, {
        types:["address"],
        componentRestrictions:{'country':'IN'}
      });
      const autocompleteDest = new google.maps.places.Autocomplete(this.searchElementRefDest.nativeElement, {
        types:["address"],
        componentRestrictions:{'country':'IN'}
      });

      autocompleteSrc.addListener('place_changed', ()=>{
        this.ngZone.run(()=>{
          const place : google.maps.places.PlaceResult=autocompleteSrc.getPlace();
          if (place.geometry === undefined || place.geometry === null ){
            return ;
          }
          this.origin = {
            lat : place.geometry.location.lat(),
            lng : place.geometry.location.lng()
          };
          this.source=place.formatted_address;
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
          //console.log("Origin ::"+this.source+ "LatLng "+this.origin);

          this.markers.push({
            lat: this.lat,
            lng: this.lng,
            label: 'O',
            draggable: true
          });
        });
      });
      autocompleteDest.addListener('place_changed', ()=>{
        this.ngZone.run(()=>{
          const place : google.maps.places.PlaceResult=autocompleteDest.getPlace();
          if (place.geometry === undefined || place.geometry === null ){
            return ;
          }
          this.dest = {
            lat : place.geometry.location.lat(),
            lng : place.geometry.location.lng()
          };
          this.destination=place.formatted_address;
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
          //console.log("Origin ::"+this.destination+ "LatLng "+this.dest.lat);

          this.markers.push({
            lat: this.lat,
            lng: this.lng,
            label: 'D',
            draggable: true
          });
        });
      });
    });
  }

  private setCurrentPosition(){
    if ( 'geolocation' in navigator ){
      navigator.geolocation.getCurrentPosition((pos)=>{
        this.lat=pos.coords.latitude;
        this.lng=pos.coords.longitude;
      });
    }
  }


  zoom: number = 8;

  // initial center position for the map
  lat: number = 17.281099;
  lng: number = 74.180603;

  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }

  mapClicked($event: MouseEvent) {
    this.markers.push({
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      draggable: true
    });
  }

  markerDragEnd(m: marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }

  markers: marker[] = [
	  // {
		//   lat: 18.520430,
		//   lng: 73.856743,
		//   label: 'A',
		//   draggable: true
	  // },
	  // {
		//   lat: 13.184570,
		//   lng: 77.479280,
		//   label: 'B',
		//   draggable: true
	  // }
  ]
  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      console.log(results);
      console.log(status);
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }

    });
  }
}

// just an interface for type safety.
interface marker {
	lat: number;
	lng: number;
	label?: string;
	draggable: boolean;
}
